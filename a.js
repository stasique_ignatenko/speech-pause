console.log(document);

let phrase;
isMute = false;
isPause = false;

let playButton = document.getElementById('play-button');
let pauseButton = document.getElementById('pause-button');
let continueButton = document.getElementById('continue-button');
let muteButton = document.getElementById('mute-button');
let stopButton = document.getElementById('stop-button');
let textInput = document.getElementById('text');
let speedInput = document.getElementById('speed');

console.log(playButton);

playButton.addEventListener('click', () => {
	playText(textInput.value);
});

pauseButton.addEventListener('click', () => {
	isPause = !isPause;
	pause();
});

continueButton.addEventListener('click', () => {
	continueFunction();
});

muteButton.addEventListener('click', () => {
	mute();
});

stopButton.addEventListener('click', () => {
	stop();
});

function playText(text) {
	phrase = new SpeechSynthesisUtterance(text);
	speechSynthesis.speak(phrase);

	phrase.onend = () => {
		console.log('end');
	};
}

function pause() {
	speechSynthesis.pause();
}

function continueFunction() {
	speechSynthesis.resume();
}

function mute() {
	isMute = !isMute;
	speechSynthesis.pause();

	phrase.onpause = () => {
		console.log('paused');

		if (!isPause) {
			if (isMute) {
				console.log('mute');
				speechSynthesis.pause();
				phrase.volume = 0;
				speechSynthesis.resume();
			} else {
				console.log('!mute');
				speechSynthesis.pause();
				phrase.volume = 1;
				speechSynthesis.resume();
			}
		}
	};
}

function stop() {
	speechSynthesis.cancel();
}
